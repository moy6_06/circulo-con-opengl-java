package org.yourorghere;
import com.sun.opengl.util.Animator;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.*;
import java.lang.Math;
import static javax.swing.JFrame.EXIT_ON_CLOSE;


public class Circulo extends JFrame{
    
    static GL gl;
    static GLU glu;
    int PI=(int) 3.141559;
    
    public Circulo (){
    setTitle("Practica Lineas");
    setSize(600,600);
    
    //instanciar la clase graphics
    Circulo.GraphicListener listener = new Circulo.GraphicListener();
    
    //Crear el canvas
    GLCanvas canvas = new GLCanvas(new GLCapabilities());
    canvas.addGLEventListener(listener);
    getContentPane().add(canvas);
    }
    
    public static void main (String args[]){
        Circulo frame = new Circulo();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public class GraphicListener implements GLEventListener{

        @Override
        public void init(GLAutoDrawable drawable) {
            
            gl = drawable.getGL();
            gl.glEnable(gl.GL_BLEND);
            gl.glBlendFunc( gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA);
            
        }

        @Override
        public void display(GLAutoDrawable drawable) {
            glu = new GLU();
            gl = drawable.getGL();            
            //gl.glClearColor(0,0,1,0);
            
            gl.glBegin(GL.GL_POLYGON);
            
            for (int i = 0; i < 10000; i++) {
                float x= (float) Math.cos(i*2*PI/100);
                float y=(float)  Math.sin(i*2*PI/100);
                
               // gl.glColor3f(0.999f,0.899f,0.100f);
                gl.glVertex2f(x,y);
            }
            
            for (int i = 0; i < 1000; i++) {
                float x= (float) Math.cos(i*2*PI/5);
                float y=(float)  Math.sin(i*2*PI/5);
                
                gl.glColor3f(0.250f,0.800f,0.458f);
                gl.glVertex2f(x,y);
            }
            gl.glEnd();
            //gl.glFlush();
        }

        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }

        @Override
        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }
    
    }
    
}
